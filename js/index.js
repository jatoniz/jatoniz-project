
    $(function() {

        $("[data-toggle='tooltip']").tooltip();
        $("[data-toggle='popover']").popover();
        $('.carousel').carousel({
          interval:3000

        });
        $('#contacto').on('show.bs.modal', function (e){
          console.log('el modal se esta mostrando');

          $('#contactoBtn').removeClass('btn-outline-success');
          $('#contactoBtn').addClass('btn-primary');
          $('#contactoBtn').prop('disabled', true);


        });
        $('#contacto').on('shown.bs.modal', function (e){
          console.log('el modal se esta mostro y bloqueo el boton');
        });
        $('#contacto').on('hide.bs.modal', function (e){
          console.log('el modal se cerro');
        });
        $('#contacto').on('hidden.bs.modal', function (e){
          console.log('el modal esta cerrado y habilito el boton');
        $('#contactoBtn').prop('disabled', false);
        $('#contactoBtn').removeClass('btn-primary');
        $('#contactoBtn').addClass('btn-outline-success');


        });

    });
    